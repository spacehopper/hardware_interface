# README #

This is the hardware interface for controlling maxon motors over ethercat.

### Installation ###

1. Clone this repository
2. Run `cd hardware_interface && catkin build`
3. Add the following to your .bashrc file: "source $HOME/git/hardware_interface/devel/setup.bash"
    (it is assumed that you cloned the directory into home/git!)
4. You can now run the hardware interface by running `roslaunch hardware_interface hardware_interface.launch`

### Setup ###



### Who do I talk to? ###

Maintainer: Joschua Wüthrich, jwuethrich@student.ethz.ch