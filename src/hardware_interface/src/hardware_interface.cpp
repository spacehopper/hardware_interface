#include "hardware_interface/EthercatDeviceConfigurator.hpp"
#include "hardware_interface/Worker.hpp"
#include <ros/ros.h>
#include "std_msgs/Float32MultiArray.h"
#include <maxon_epos_ethercat_sdk/Maxon.hpp>
#include "yaml-cpp/yaml.h"
#include <filesystem>
#include <thread>




/*
** Program entry. setup.yaml is passed as an argument in the roslaunch file.
 */
int main(int argc, char**argv)
{   
    
    ros::init(argc, argv, "hardware_interface");

    if(argc < 2) {
        std::cerr << "Please pass a mode to the run.sh script via the -m flag" << std::endl;
        return EXIT_FAILURE;
    }
    
    //instantiate the worker
    Worker worker = Worker(argv[1]);

    worker.setup_slaves();

    worker.start_ros_worker();
}
