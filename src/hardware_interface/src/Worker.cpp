#include <ros/ros.h>
#include "std_msgs/Float32MultiArray.h"
#include <sensor_msgs/JointState.h>
#include <spacehopper_msgs/ActuatorState.h>
#include <spacehopper_msgs/LegActuatorState.h>
#include <spacehopper_msgs/RobotActuatorState.h>
#include <maxon_epos_ethercat_sdk/Maxon.hpp>
#include "hardware_interface/EthercatDeviceConfigurator.hpp"
#include <vector>
#include <hardware_interface/Worker.hpp>
#include <string>
#include <filesystem>
#include <stdexcept>

//constructor
Worker::Worker(std::string commandline_argument) {
    //member variables for saving demanded states
    act_torques_robot = std::vector<float>(9,0);
    act_torques_leg = std::vector<float>(3,0);
    demanded_act_pos= std::vector<float>(9,0);
    demanded_actuator_state_robot = spacehopper_msgs::RobotActuatorState();
    demanded_actuator_state_leg = spacehopper_msgs::LegActuatorState();
    commandline_arg = commandline_argument;

    //parse the commandline argument
    std::string pre_path = config_path = std::filesystem::current_path().string() + "/src/hardware_interface/src/hardware_interface/config_setup";
    if (commandline_argument == "robot_tc_stdmsgs") {
        mode = commandline_argument;
        config_path = pre_path + "/robot_setup.yaml";
    } else if (commandline_argument == "robot_pc_sphmsgs") {
        mode = commandline_argument;
        config_path = pre_path + "/robot_setup.yaml";
    } else if (commandline_argument == "robot_pc_stdmsgs") {
        mode = commandline_argument;
        config_path = pre_path + "/robot_setup.yaml";
    } else if (commandline_argument == "leg_tc_stdmsgs") {
        mode = commandline_argument;
        config_path = pre_path + "/leg_setup.yaml";
    } else if (commandline_argument == "leg_pc_sphmsgs") {
        mode = commandline_argument;
        config_path = pre_path + "/leg_setup.yaml";
    } else if (commandline_argument == "leg_pc_stdmsgs") {
        mode = commandline_argument;
        config_path = pre_path + "/leg_setup.yaml";
    } else if (commandline_argument == "leg_pc_delay") {
        mode = commandline_argument;
        config_path = pre_path + "/leg_setup.yaml";
    } else {
        std::cerr << "Your chosen mode is not available" << std::endl;
        throw std::invalid_argument("Your chosen mode is not available");
    }

    //check if parameter server is available and parse frequency
    if (! n.hasParam("frequencies/hardware_interface")) {
        throw std::invalid_argument("Please make sure your ROS parameter server is running and contains the parameter frequencies/hardware_interface");
    }
    n.getParam("frequencies/hardware_interface", frequency_hardware_interface);

    parse_ethercat_setup_file();

    //print frequencies
    MELO_INFO_STREAM("Running the ROS Loop at " + std::to_string(frequency_hardware_interface) + " and the Ethercat Communication at " + std::to_string(1/time_step_ethercat_communication));
    
    //a new EthercatDeviceConfigurator object (path to setup.yaml as constructor argument)
    configurator = std::make_shared<EthercatDeviceConfigurator>(config_path);
}

//parser methods
void Worker::parse_ethercat_setup_file() {
    std::string pre_path = std::filesystem::current_path().string() + "/src/hardware_interface/src/hardware_interface/config_setup";
    
    //parse frequency from setup file for ethercat master
    YAML::Node node_master = YAML::LoadFile(config_path);
    time_step_ethercat_communication = node_master["ethercat_master"]["time_step"].as<double>();
    if (frequency_hardware_interface != 1/time_step_ethercat_communication) {
        MELO_WARN_STREAM("Your specified hardware_interface frequency and time_step do not match!")
    }

    YAML::Node node_hipmotor = YAML::LoadFile(pre_path + "/device_configurations/maxonhip.yaml");
    YAML::Node node_kneemotor = YAML::LoadFile(pre_path + "/device_configurations/maxonknee.yaml");
    std::vector<std::string> hip_mode = node_hipmotor["Hardware"]["mode_of_operation"].as<std::vector<std::string>>();
    std::vector<std::string> knee_mode = node_hipmotor["Hardware"]["mode_of_operation"].as<std::vector<std::string>>();
    
    //check that only one mode is specified
    if (hip_mode.size() > 1) {
        throw std::invalid_argument("Please make sure your that you only specify one mode of operation in your maxonhip.yaml file");
    }
    if (knee_mode.size() > 1) {
        throw std::invalid_argument("Please make sure your that you only specify one mode of operation in your maxonknee.yaml file");
    }

    //check if knee mode is the same as hip mode
    if(knee_mode[0]!=hip_mode[0]){
        throw std::invalid_argument("Please make sure your that your mode of operation in maxonknee.yaml and maxonhip.yaml is the same");    
    }

    //check that the correct mode is specified
    if (hip_mode[0] == "CyclicSynchronousPositionMode"){
        if(commandline_arg == "leg_tc_stdmsgs" || commandline_arg=="robot_tc_stdmsgs"){
            throw std::invalid_argument("Your worker is in CyclicSynchronousTorqueMode but your maxonknee.yaml and maxonhip.yaml has CyclicSynchronousPositionMode enabled");
        }
    }

    if (hip_mode[0] == "CyclicSynchronousTorqueMode"){
        if(commandline_arg != "leg_tc_stdmsgs" && commandline_arg!="robot_tc_stdmsgs"){
            throw std::invalid_argument("Your worker is in CyclicSynchronousPositionMode but your maxonknee.yaml and maxonhip.yaml has CyclicSynchronousTorqueMode enabled");
        }
    }   
}

//start method
void Worker::start_ros_worker() {
    if (mode == "robot_tc_stdmsgs") {
        this->robot_tc_stdmsgs();
    } else if (mode == "robot_pc_sphmsgs") {
        this->robot_pc_spacehoppermsgs();
    } else if (mode == "robot_pc_stdmsgs") {
        this->robot_pc_stdmsgs();
    } else if (mode == "leg_tc_stdmsgs") {
        this->leg_tc_stdmsgs();
    } else if (mode == "leg_pc_sphmsgs") {
        this->leg_pc_spacehoppermsgs();
    } else if (mode == "leg_pc_stdmsgs") {
        this->leg_pc_stdmsgs();
    } else if (mode == "leg_pc_delay") {
        this->leg_pc_delay();
    } else {
        throw std::invalid_argument("The mode membervariable has changed since construction which is invalid");
    }
}

//pdo communication methods
void Worker::setup_slaves() {
    /*
    ** Start all masters.
    ** There is exactly one bus per master which is also started.
    ** All online (i.e. SDO) configuration is done during this call.
    ** The EtherCAT interface is active afterwards, all drives are in Operational
    ** EtherCAT state and PDO communication may begin.
     */
    for(auto & master: configurator->getMasters())
    {
        if(!master->startup())
        {
            std::cerr << "Startup not successful." << std::endl;
            return;
        }
    }

    ethercatUpdateThread_.reset(new std::thread([this](){
        update_master();
    }));

    MELO_INFO_STREAM("[HardwareInterface] Started Ethercat Worker")

    //cast slave pointer into maxon pointers
    for(const auto & slave:configurator->getSlaves()) {
        slaves.push_back(std::dynamic_pointer_cast<maxon::Maxon>(slave));
    }

    //enable all slaves
    for(const auto& maxon_slave_ptr: slaves) {
        maxon_slave_ptr->setDriveStateViaPdo(maxon::DriveState::OperationEnabled, true);
    }
    
    MELO_INFO_STREAM("[HardwareInterface] Ethercat Setup succesfull");
}

void Worker::shutdown(){
    MELO_INFO_STREAM("[Worker] shutdown ethercat Worker.")


    for(const auto& maxon_slave_ptr: slaves) {
       maxon_slave_ptr->setDriveStateViaPdo(maxon::DriveState::SwitchOnDisabled, true); 
       MELO_INFO_STREAM("[HardwareInterface] disabled slave: " << maxon_slave_ptr->getName())
    }

    abortEthercatUpdate_ = true;
    ethercatUpdateThread_->join();
    for (const auto &master : configurator->getMasters())
    {
        master->shutdown();
    }
}

void Worker::update_master()
{

    //set realtime priority
    bool rtSuccess = true;
    for(const auto & master: configurator->getMasters())
    {
        rtSuccess &= master->setRealtimePriority(48);
    }
    std::cout << "Setting RT Priority: " << (rtSuccess? "successful." : "not successful. Check user privileges.") << std::endl;
    

    while (!abortEthercatUpdate_)
    {
        for (const auto &master : configurator->getMasters())
        {
            master->update(ecat_master::UpdateMode::StandaloneEnforceStep);
        }
    }
};

//callbacks for subscribers
void Worker::callback_robot_stdmsgs(const std_msgs::Float32MultiArray::ConstPtr &msg) {
    act_torques_robot = msg->data;
}

void Worker::callback_robot_spacehoppermsgs(const spacehopper_msgs::RobotActuatorState msg) {
    demanded_actuator_state_robot = msg;
}

void Worker::callback_leg_stdmsgs(const std_msgs::Float32MultiArray::ConstPtr &msg) {
    act_torques_leg = msg->data;
}

void Worker::callback_pc_stdmsgs(const std_msgs::Float32MultiArray::ConstPtr &msg) {
    uint i = 0;
    for (const auto & element:msg->data) {
        demanded_act_pos[i] = element;
        i++;
    }
}

void Worker::callback_leg_spacehoppermsgs(const spacehopper_msgs::LegActuatorState msg) {
    demanded_actuator_state_leg = msg;
}

//worker methods
void Worker::robot_tc_stdmsgs() {
    
    ros::Subscriber sub = n.subscribe("act_torques", 1000, &Worker::callback_robot_stdmsgs, this);
    ros::Publisher pub = n.advertise<std_msgs::Float32MultiArray>("act_states",1000);
    ros::Rate loop_rate(frequency_hardware_interface);
   //main loop
    while(ros::ok()){
        // instantiate all messages
        std_msgs::Float32MultiArray msg1;
        std::vector<float> act_states(18);
        for(const auto & maxon_slave_ptr:slaves)  {
  
                // -------------------------- Leg1 ----------------------------------------------------------//

                if(maxon_slave_ptr->getName()=="Maxonhip11"){
                if (maxon_slave_ptr->lastPdoStateChangeSuccessful() &&
                        maxon_slave_ptr->getReading().getDriveState() == maxon::DriveState::OperationEnabled)
                    {
                        
                        maxon::Command command;
                        command.setModeOfOperation(maxon::ModeOfOperationEnum::CyclicSynchronousTorqueMode);
                        auto reading = maxon_slave_ptr->getReading();
                        command.setTargetTorque(act_torques_robot[0]);

                        maxon_slave_ptr->stageCommand(command);
                        act_states[0] = reading.getActualPosition();
                        act_states[3] = reading.getActualVelocityAveraged();
                    }
                }
                else if(maxon_slave_ptr->getName()=="Maxonhip12"){
                if (maxon_slave_ptr->lastPdoStateChangeSuccessful() &&
                        maxon_slave_ptr->getReading().getDriveState() == maxon::DriveState::OperationEnabled)
                    {
                        
                        maxon::Command command;
                        command.setModeOfOperation(maxon::ModeOfOperationEnum::CyclicSynchronousTorqueMode);
                        auto reading = maxon_slave_ptr->getReading();
                        command.setTargetTorque(act_torques_robot[1]);
                        maxon_slave_ptr->stageCommand(command);
                        act_states[1] = reading.getActualPosition();
                        act_states[4] = reading.getActualVelocityAveraged();
                    }
                }
                else if(maxon_slave_ptr->getName()=="Maxonknee13"){
                if (maxon_slave_ptr->lastPdoStateChangeSuccessful() &&
                        maxon_slave_ptr->getReading().getDriveState() == maxon::DriveState::OperationEnabled)
                    {
                        maxon::Command command;
                        command.setModeOfOperation(maxon::ModeOfOperationEnum::CyclicSynchronousTorqueMode);
                        auto reading = maxon_slave_ptr->getReading();
                        command.setTargetTorque(act_torques_robot[2]);
                        maxon_slave_ptr->stageCommand(command);
                        act_states[2] = reading.getActualPosition();
                        act_states[5] = reading.getActualVelocityAveraged();
                    }
                }

                // -------------------------- Leg2 ----------------------------------------------------------//

                else if(maxon_slave_ptr->getName()=="Maxonhip21"){
                if (maxon_slave_ptr->lastPdoStateChangeSuccessful() &&
                        maxon_slave_ptr->getReading().getDriveState() == maxon::DriveState::OperationEnabled)
                    {
                        maxon::Command command;
                        command.setModeOfOperation(maxon::ModeOfOperationEnum::CyclicSynchronousTorqueMode);
                        auto reading = maxon_slave_ptr->getReading();
                        command.setTargetTorque(act_torques_robot[3]);
                        maxon_slave_ptr->stageCommand(command);
                        act_states[6] = reading.getActualPosition();
                        act_states[9] = reading.getActualVelocityAveraged();
                    }
                }
                else if(maxon_slave_ptr->getName()=="Maxonhip22"){
                if (maxon_slave_ptr->lastPdoStateChangeSuccessful() &&
                        maxon_slave_ptr->getReading().getDriveState() == maxon::DriveState::OperationEnabled)
                    {
                        maxon::Command command;
                        command.setModeOfOperation(maxon::ModeOfOperationEnum::CyclicSynchronousTorqueMode);
                        auto reading = maxon_slave_ptr->getReading();
                        command.setTargetTorque(act_torques_robot[4]);
                        maxon_slave_ptr->stageCommand(command);
                        act_states[7] = reading.getActualPosition();
                        act_states[10] = reading.getActualVelocityAveraged();
                    }
                }
                else if(maxon_slave_ptr->getName()=="Maxonknee23"){
                if (maxon_slave_ptr->lastPdoStateChangeSuccessful() &&
                        maxon_slave_ptr->getReading().getDriveState() == maxon::DriveState::OperationEnabled)
                    {
                        maxon::Command command;
                        command.setModeOfOperation(maxon::ModeOfOperationEnum::CyclicSynchronousTorqueMode);
                        auto reading = maxon_slave_ptr->getReading();
                        command.setTargetTorque(act_torques_robot[5]);
                        maxon_slave_ptr->stageCommand(command);
                        act_states[8] = reading.getActualPosition();
                        act_states[11] = reading.getActualVelocityAveraged();
                    }
                }

                // -------------------------- Leg3 ----------------------------------------------------------//

                else if(maxon_slave_ptr->getName()=="Maxonhip31"){
                if (maxon_slave_ptr->lastPdoStateChangeSuccessful() &&
                        maxon_slave_ptr->getReading().getDriveState() == maxon::DriveState::OperationEnabled)
                    {
                        
                        maxon::Command command;
                        command.setModeOfOperation(maxon::ModeOfOperationEnum::CyclicSynchronousTorqueMode);
                        auto reading = maxon_slave_ptr->getReading();
                        command.setTargetTorque(act_torques_robot[6]);

                        maxon_slave_ptr->stageCommand(command);
                        act_states[12] = reading.getActualPosition();
                        act_states[15] = reading.getActualVelocityAveraged();
                    }
                }
                else if(maxon_slave_ptr->getName()=="Maxonhip32"){
                if (maxon_slave_ptr->lastPdoStateChangeSuccessful() &&
                        maxon_slave_ptr->getReading().getDriveState() == maxon::DriveState::OperationEnabled)
                    {
                        
                        maxon::Command command;
                        command.setModeOfOperation(maxon::ModeOfOperationEnum::CyclicSynchronousTorqueMode);
                        auto reading = maxon_slave_ptr->getReading();
                        command.setTargetTorque(act_torques_robot[7]);
                        maxon_slave_ptr->stageCommand(command);
                        act_states[13] = reading.getActualPosition();
                        act_states[16] = reading.getActualVelocityAveraged();
                    }
                }
                else if(maxon_slave_ptr->getName()=="Maxonknee33"){
                if (maxon_slave_ptr->lastPdoStateChangeSuccessful() &&
                        maxon_slave_ptr->getReading().getDriveState() == maxon::DriveState::OperationEnabled)
                    {
                        maxon::Command command;
                        command.setModeOfOperation(maxon::ModeOfOperationEnum::CyclicSynchronousTorqueMode);
                        auto reading = maxon_slave_ptr->getReading();
                        command.setTargetTorque(act_torques_robot[8]);
                        maxon_slave_ptr->stageCommand(command);
                        act_states[14] = reading.getActualPosition();
                        act_states[17] = reading.getActualVelocityAveraged();
                    }
                }
                else
                {
                    MELO_WARN_STREAM("Maxon '" << maxon_slave_ptr->getName()
                                                                        << "': " << maxon_slave_ptr->getReading().getDriveState());
                }
        }
        msg1.data = act_states;
        pub.publish(msg1);
        ros::spinOnce();
        loop_rate.sleep();
    }
    shutdown();
}

void Worker::robot_pc_spacehoppermsgs() {

    ros::Subscriber sub = n.subscribe("demanded_actuator_positions", 1000, &Worker::callback_robot_spacehoppermsgs, this);
    ros::Publisher pub = n.advertise<spacehopper_msgs::RobotActuatorState>("act_states",1000);
    ros::Rate loop_rate(frequency_hardware_interface);
    ros::spinOnce();

    /*
    ** The communication update loop.
    ** This loop is supposed to be executed at a constant rate.
    ** The EthercatMaster::update function incorporates a mechanism
    ** to create a constant rate.
     */
   
    while(ros::ok()){
        //create actuator state message
        spacehopper_msgs::RobotActuatorState robot_actuator_state;
        robot_actuator_state.demanded = false;

        robot_actuator_state.leg1.demanded = false;
        robot_actuator_state.leg2.demanded = false;
        robot_actuator_state.leg3.demanded = false;

        /*
        ** Here are the commands coded which are sent to the actuators
        */
        for(const auto & maxon_slave_ptr:slaves) 
        {
            //====================== Leg 1 ====================
            
            if(maxon_slave_ptr->getName()=="Maxonhip11"){
            
                if (maxon_slave_ptr->lastPdoStateChangeSuccessful() &&
                        maxon_slave_ptr->getReading().getDriveState() == maxon::DriveState::OperationEnabled)
                    {
                        //setup command
                        maxon::Command command;
                        command.setModeOfOperation(maxon::ModeOfOperationEnum::CyclicSynchronousPositionMode);
                        command.setTargetPosition(demanded_actuator_state_robot.leg1.actuator1.position);

                        //get actuator states
                        auto reading = maxon_slave_ptr->getReading();
                        
                        //send command to motor
                        maxon_slave_ptr->stageCommand(command);
                        
                        //save actuator states
                        spacehopper_msgs::ActuatorState actuator1;

                        actuator1.actuator_index = 1;

                        actuator1.position = reading.getActualPosition();
                        actuator1.velocity = reading.getActualVelocity();
                        actuator1.velocity_averaged = reading.getActualVelocityAveraged();
                        
                        actuator1.actual_current = reading.getActualCurrent();
                        actuator1.actual_torque = reading.getActualTorque();

                        robot_actuator_state.leg1.actuator1 = actuator1;
                    }
            }

            else if(maxon_slave_ptr->getName()=="Maxonhip12"){
            
                if (maxon_slave_ptr->lastPdoStateChangeSuccessful() &&
                        maxon_slave_ptr->getReading().getDriveState() == maxon::DriveState::OperationEnabled)
                    {
                        //setup command
                        maxon::Command command;
                        command.setModeOfOperation(maxon::ModeOfOperationEnum::CyclicSynchronousPositionMode);
                        command.setTargetPosition(demanded_actuator_state_robot.leg1.actuator2.position);
                        
                        //get actuator states
                        auto reading = maxon_slave_ptr->getReading();
                        
                        //send command to motor
                        maxon_slave_ptr->stageCommand(command);

                        //save actuator states
                        spacehopper_msgs::ActuatorState actuator2;

                        actuator2.actuator_index = 2;
                        
                        actuator2.position = reading.getActualPosition();
                        actuator2.velocity = reading.getActualVelocity();
                        actuator2.velocity_averaged = reading.getActualVelocityAveraged();
                        
                        actuator2.actual_current = reading.getActualCurrent();
                        actuator2.actual_torque = reading.getActualTorque();

                        robot_actuator_state.leg1.actuator2 = actuator2;
                    }
            }

            else if(maxon_slave_ptr->getName()=="Maxonknee13"){
            
                if (maxon_slave_ptr->lastPdoStateChangeSuccessful() &&
                        maxon_slave_ptr->getReading().getDriveState() == maxon::DriveState::OperationEnabled)
                    {
                        //setup command
                        maxon::Command command;
                        command.setModeOfOperation(maxon::ModeOfOperationEnum::CyclicSynchronousPositionMode);
                        command.setTargetPosition(demanded_actuator_state_robot.leg1.actuator3.position);

                        //get actuator states
                        auto reading = maxon_slave_ptr->getReading();
                        
                        //send command to motor
                        maxon_slave_ptr->stageCommand(command);
                        
                        //save actuator states
                        spacehopper_msgs::ActuatorState actuator3;
                        
                        actuator3.actuator_index = 3;

                        actuator3.position = reading.getActualPosition();
                        actuator3.velocity = reading.getActualVelocityAveraged();
                        actuator3.velocity_averaged = reading.getActualVelocityAveraged();
                        
                        actuator3.actual_current = reading.getActualCurrent();
                        actuator3.actual_torque = reading.getActualTorque();

                        robot_actuator_state.leg1.actuator3 = actuator3;
                    }
            }

            //================== Leg 2 ==================

            else if(maxon_slave_ptr->getName()=="Maxonhip21"){
            
                if (maxon_slave_ptr->lastPdoStateChangeSuccessful() &&
                        maxon_slave_ptr->getReading().getDriveState() == maxon::DriveState::OperationEnabled)
                    {
                        //setup command
                        maxon::Command command;
                        command.setModeOfOperation(maxon::ModeOfOperationEnum::CyclicSynchronousPositionMode);
                        command.setTargetPosition(demanded_actuator_state_robot.leg2.actuator1.position);

                        //get actuator states
                        auto reading = maxon_slave_ptr->getReading();
                        
                        //send command to motor
                        maxon_slave_ptr->stageCommand(command);
                        
                        //save actuator states
                        spacehopper_msgs::ActuatorState actuator1;
                        
                        actuator1.actuator_index = 1;

                        actuator1.position = reading.getActualPosition();
                        actuator1.velocity = reading.getActualVelocityAveraged();
                        actuator1.velocity_averaged = reading.getActualVelocityAveraged();
                        
                        actuator1.actual_current = reading.getActualCurrent();
                        actuator1.actual_torque = reading.getActualTorque();

                        robot_actuator_state.leg2.actuator1 = actuator1;
                    }
            } 

            else if(maxon_slave_ptr->getName()=="Maxonhip22"){
                
                if (maxon_slave_ptr->lastPdoStateChangeSuccessful() &&
                        maxon_slave_ptr->getReading().getDriveState() == maxon::DriveState::OperationEnabled)
                    {
                        //setup command
                        maxon::Command command;
                        command.setModeOfOperation(maxon::ModeOfOperationEnum::CyclicSynchronousPositionMode);
                        command.setTargetPosition(demanded_actuator_state_robot.leg2.actuator2.position);

                        //get actuator states
                        auto reading = maxon_slave_ptr->getReading();
                        
                        //send command to motor
                        maxon_slave_ptr->stageCommand(command);
                        
                        //save actuator states
                        spacehopper_msgs::ActuatorState actuator2;
                        
                        actuator2.actuator_index = 2;

                        actuator2.position = reading.getActualPosition();
                        actuator2.velocity = reading.getActualVelocityAveraged();
                        actuator2.velocity_averaged = reading.getActualVelocityAveraged();
                        
                        actuator2.actual_current = reading.getActualCurrent();
                        actuator2.actual_torque = reading.getActualTorque();

                        robot_actuator_state.leg2.actuator2 = actuator2;
                    }
            } 

            else if(maxon_slave_ptr->getName()=="Maxonknee23"){
            
                if (maxon_slave_ptr->lastPdoStateChangeSuccessful() &&
                        maxon_slave_ptr->getReading().getDriveState() == maxon::DriveState::OperationEnabled)
                    {
                        //setup command
                        maxon::Command command;
                        command.setModeOfOperation(maxon::ModeOfOperationEnum::CyclicSynchronousPositionMode);
                        command.setTargetPosition(demanded_actuator_state_robot.leg2.actuator3.position);

                        //get actuator states
                        auto reading = maxon_slave_ptr->getReading();
                        
                        //send command to motor
                        maxon_slave_ptr->stageCommand(command);
                        
                        //save actuator states
                        spacehopper_msgs::ActuatorState actuator3;
                        
                        actuator3.actuator_index = 3;

                        actuator3.position = reading.getActualPosition();
                        actuator3.velocity = reading.getActualVelocityAveraged();
                        actuator3.velocity_averaged = reading.getActualVelocityAveraged();
                        
                        actuator3.actual_current = reading.getActualCurrent();
                        actuator3.actual_torque = reading.getActualTorque();

                        robot_actuator_state.leg2.actuator3 = actuator3;
                    }
            } 

            //================== Leg 3 ==================

            else if(maxon_slave_ptr->getName()=="Maxonhip31"){
            
                if (maxon_slave_ptr->lastPdoStateChangeSuccessful() &&
                        maxon_slave_ptr->getReading().getDriveState() == maxon::DriveState::OperationEnabled)
                    {
                        //setup command
                        maxon::Command command;
                        command.setModeOfOperation(maxon::ModeOfOperationEnum::CyclicSynchronousPositionMode);
                        command.setTargetPosition(demanded_actuator_state_robot.leg3.actuator1.position);

                        //get actuator states
                        auto reading = maxon_slave_ptr->getReading();
                        
                        //send command to motor
                        maxon_slave_ptr->stageCommand(command);
                        
                        //save actuator states
                        spacehopper_msgs::ActuatorState actuator1;
                        
                        actuator1.actuator_index = 1;

                        actuator1.position = reading.getActualPosition();
                        actuator1.velocity = reading.getActualVelocityAveraged();
                        actuator1.velocity_averaged = reading.getActualVelocityAveraged();
                        
                        actuator1.actual_current = reading.getActualCurrent();
                        actuator1.actual_torque = reading.getActualTorque();

                        robot_actuator_state.leg3.actuator1 = actuator1;
                    }
            } 

            else if(maxon_slave_ptr->getName()=="Maxonhip32"){
            
                if (maxon_slave_ptr->lastPdoStateChangeSuccessful() &&
                        maxon_slave_ptr->getReading().getDriveState() == maxon::DriveState::OperationEnabled)
                    {
                        //setup command
                        maxon::Command command;
                        command.setModeOfOperation(maxon::ModeOfOperationEnum::CyclicSynchronousPositionMode);
                        command.setTargetPosition(demanded_actuator_state_robot.leg3.actuator2.position);

                        //get actuator states
                        auto reading = maxon_slave_ptr->getReading();
                        
                        //send command to motor
                        maxon_slave_ptr->stageCommand(command);
                        
                        //save actuator states
                        spacehopper_msgs::ActuatorState actuator2;
                        actuator2.actuator_index = 2;

                        actuator2.position = reading.getActualPosition();
                        actuator2.velocity = reading.getActualVelocityAveraged();
                        actuator2.velocity_averaged = reading.getActualVelocityAveraged();
                        
                        actuator2.actual_current = reading.getActualCurrent();
                        actuator2.actual_torque = reading.getActualTorque();

                        robot_actuator_state.leg3.actuator2 = actuator2;
                    }
            } 

            else if(maxon_slave_ptr->getName()=="Maxonknee33"){
            
                if (maxon_slave_ptr->lastPdoStateChangeSuccessful() &&
                        maxon_slave_ptr->getReading().getDriveState() == maxon::DriveState::OperationEnabled)
                    {
                        //setup command
                        maxon::Command command;
                        command.setModeOfOperation(maxon::ModeOfOperationEnum::CyclicSynchronousPositionMode);
                        command.setTargetPosition(demanded_actuator_state_robot.leg3.actuator3.position);

                        //get actuator states
                        auto reading = maxon_slave_ptr->getReading();
                        
                        //send command to motor
                        maxon_slave_ptr->stageCommand(command);
                        
                        //save actuator states
                        spacehopper_msgs::ActuatorState actuator3;
                        
                        actuator3.actuator_index = 3;

                        actuator3.position = reading.getActualPosition();
                        actuator3.velocity = reading.getActualVelocityAveraged();
                        actuator3.velocity_averaged = reading.getActualVelocityAveraged();
                        
                        actuator3.actual_current = reading.getActualCurrent();
                        actuator3.actual_torque = reading.getActualTorque();

                        robot_actuator_state.leg3.actuator3 = actuator3;
                    }
            } 

            else
            {
                MELO_WARN_STREAM("Maxon '" << maxon_slave_ptr->getName() << "': " << maxon_slave_ptr->getReading().getDriveState());
            }

        }
        pub.publish(robot_actuator_state);
        ros::spinOnce();
        loop_rate.sleep();
    }
    shutdown();
}

void Worker::robot_pc_stdmsgs(){

    ros::Subscriber sub = n.subscribe("demanded_act_pos", 10, &Worker::callback_pc_stdmsgs, this);
    ros::Publisher pub = n.advertise<std_msgs::Float32MultiArray>("act_states", 10);
    ros::Rate loop_rate(frequency_hardware_interface);
   
   //main loop
   while(ros::ok()){
        //create actuator state message
        std_msgs::Float32MultiArray msg1;
        std::vector<float> act_states(18);
        /*
        ** Here are the commands coded which are sent to the actuators
        */
        for(const auto & maxon_slave_ptr:slaves) 
        {
            if(maxon_slave_ptr->getName()=="Maxonhip11"){  
            if (maxon_slave_ptr->lastPdoStateChangeSuccessful() &&
                    maxon_slave_ptr->getReading().getDriveState() == maxon::DriveState::OperationEnabled)
                {
                    //setup command
                    maxon::Command command;
                    command.setModeOfOperation(maxon::ModeOfOperationEnum::CyclicSynchronousPositionMode);
                    command.setTargetPosition(demanded_act_pos[0]);

                    //get actuator states
                    auto reading = maxon_slave_ptr->getReading();
                    
                    //send command to motor
                    maxon_slave_ptr->stageCommand(command);
                    act_states[0] = reading.getActualPosition();
                    act_states[3] = reading.getActualVelocityAveraged();
                }
            }
            else if(maxon_slave_ptr->getName()=="Maxonhip12"){
            if (maxon_slave_ptr->lastPdoStateChangeSuccessful() &&
                    maxon_slave_ptr->getReading().getDriveState() == maxon::DriveState::OperationEnabled)
                {
                        //setup command
                    maxon::Command command;
                    command.setModeOfOperation(maxon::ModeOfOperationEnum::CyclicSynchronousPositionMode);
                    command.setTargetPosition(demanded_act_pos[1]);

                    //get actuator states
                    auto reading = maxon_slave_ptr->getReading();
                    
                    //send command to motor
                    maxon_slave_ptr->stageCommand(command);
                    act_states[1] = reading.getActualPosition();
                    act_states[4] = reading.getActualVelocityAveraged();
                }
            }
            else if(maxon_slave_ptr->getName()=="Maxonknee13"){
            if (maxon_slave_ptr->lastPdoStateChangeSuccessful() &&
                    maxon_slave_ptr->getReading().getDriveState() == maxon::DriveState::OperationEnabled)
                { 
                    //setup command
                    maxon::Command command;
                    command.setModeOfOperation(maxon::ModeOfOperationEnum::CyclicSynchronousPositionMode);
                    command.setTargetPosition(demanded_act_pos[2]);

                    //get actuator states
                    auto reading = maxon_slave_ptr->getReading();
                    
                    //send command to motor
                    maxon_slave_ptr->stageCommand(command);
                    act_states[2] = reading.getActualPosition();
                    act_states[5] = reading.getActualVelocityAveraged();
                }
            }

            // -------------------------- Leg2 ----------------------------------------------------------//

            else if(maxon_slave_ptr->getName()=="Maxonhip21"){
            if (maxon_slave_ptr->lastPdoStateChangeSuccessful() &&
                    maxon_slave_ptr->getReading().getDriveState() == maxon::DriveState::OperationEnabled)
                {
                    maxon::Command command;
                    command.setModeOfOperation(maxon::ModeOfOperationEnum::CyclicSynchronousPositionMode);
                    auto reading = maxon_slave_ptr->getReading();
                    command.setTargetPosition(demanded_act_pos[3]);
                    maxon_slave_ptr->stageCommand(command);
                    act_states[6] = reading.getActualPosition();
                    act_states[9] = reading.getActualVelocityAveraged();
                }
            }
            else if(maxon_slave_ptr->getName()=="Maxonhip22"){
            if (maxon_slave_ptr->lastPdoStateChangeSuccessful() &&
                    maxon_slave_ptr->getReading().getDriveState() == maxon::DriveState::OperationEnabled)
                {
                    maxon::Command command;
                    command.setModeOfOperation(maxon::ModeOfOperationEnum::CyclicSynchronousPositionMode);
                    auto reading = maxon_slave_ptr->getReading();
                    command.setTargetPosition(demanded_act_pos[4]);
                    maxon_slave_ptr->stageCommand(command);
                    act_states[7] = reading.getActualPosition();
                    act_states[10] = reading.getActualVelocityAveraged();
                }
            }
            else if(maxon_slave_ptr->getName()=="Maxonknee23"){
            if (maxon_slave_ptr->lastPdoStateChangeSuccessful() &&
                    maxon_slave_ptr->getReading().getDriveState() == maxon::DriveState::OperationEnabled)
                {
                    maxon::Command command;
                    command.setModeOfOperation(maxon::ModeOfOperationEnum::CyclicSynchronousPositionMode);
                    auto reading = maxon_slave_ptr->getReading();
                    command.setTargetPosition(demanded_act_pos[5]);
                    maxon_slave_ptr->stageCommand(command);
                    act_states[8] = reading.getActualPosition();
                    act_states[11] = reading.getActualVelocityAveraged();
                }
            }

            // -------------------------- Leg3 ----------------------------------------------------------//

            else if(maxon_slave_ptr->getName()=="Maxonhip31"){
            if (maxon_slave_ptr->lastPdoStateChangeSuccessful() &&
                    maxon_slave_ptr->getReading().getDriveState() == maxon::DriveState::OperationEnabled)
                {
                    
                    maxon::Command command;
                    command.setModeOfOperation(maxon::ModeOfOperationEnum::CyclicSynchronousPositionMode);
                    auto reading = maxon_slave_ptr->getReading();
                    command.setTargetPosition(demanded_act_pos[6]);

                    maxon_slave_ptr->stageCommand(command);
                    act_states[12] = reading.getActualPosition();
                    act_states[15] = reading.getActualVelocityAveraged();
                }
            }
            else if(maxon_slave_ptr->getName()=="Maxonhip32"){
            if (maxon_slave_ptr->lastPdoStateChangeSuccessful() &&
                    maxon_slave_ptr->getReading().getDriveState() == maxon::DriveState::OperationEnabled)
                {
                    
                    maxon::Command command;
                    command.setModeOfOperation(maxon::ModeOfOperationEnum::CyclicSynchronousPositionMode);
                    auto reading = maxon_slave_ptr->getReading();
                    command.setTargetPosition(demanded_act_pos[7]);
                    maxon_slave_ptr->stageCommand(command);
                    act_states[13] = reading.getActualPosition();
                    act_states[16] = reading.getActualVelocityAveraged();
                }
            }
            else if(maxon_slave_ptr->getName()=="Maxonknee33"){
            if (maxon_slave_ptr->lastPdoStateChangeSuccessful() &&
                    maxon_slave_ptr->getReading().getDriveState() == maxon::DriveState::OperationEnabled)
                {
                    maxon::Command command;
                    command.setModeOfOperation(maxon::ModeOfOperationEnum::CyclicSynchronousPositionMode);
                    auto reading = maxon_slave_ptr->getReading();
                    command.setTargetPosition(demanded_act_pos[8]);
                    maxon_slave_ptr->stageCommand(command);
                    act_states[14] = reading.getActualPosition();
                    act_states[17] = reading.getActualVelocityAveraged();
                }
            }

            else
            {
                MELO_WARN_STREAM("Maxon '" << maxon_slave_ptr->getName()
                                                                    << "': " << maxon_slave_ptr->getReading().getDriveState());
            }
        }
        msg1.data = act_states;
        pub.publish(msg1);
        ros::spinOnce();
        loop_rate.sleep();
    }
    shutdown();
}

void Worker::leg_tc_stdmsgs() {

    //ros setup
    ros::Subscriber sub = n.subscribe("act_torques", 1000, &Worker::callback_leg_stdmsgs, this);
    ros::Publisher pub = n.advertise<std_msgs::Float32MultiArray>("act_states", 1000);
    ros::Rate loop_rate(frequency_hardware_interface);

    //mainloop
    while(ros::ok()){
        // instantiate all messages
        std_msgs::Float32MultiArray msg1;
        std::vector<float> act_states(6);

        /*
        ** Here are the commands coded which are sent to the actuators
        */
        for(const auto & maxon_slave_ptr:slaves) {
            if(maxon_slave_ptr->getName()=="Maxonhip41") {
                if(maxon_slave_ptr->lastPdoStateChangeSuccessful() &&
                            maxon_slave_ptr->getReading().getDriveState() == maxon::DriveState::OperationEnabled) {
                            
                        maxon::Command command;
                        command.setModeOfOperation(maxon::ModeOfOperationEnum::CyclicSynchronousTorqueMode);
                        auto reading = maxon_slave_ptr->getReading();
                        command.setTargetTorque(act_torques_leg[0]);

                        maxon_slave_ptr->stageCommand(command);
                        act_states[0] = reading.getActualPosition();
                        act_states[3] = reading.getActualVelocityAveraged();
                
                } else {
                    std::cout << maxon_slave_ptr->getName() << " is not in correct DriveState or the last PDO change was not succesfull" << std::endl;
                }
            } else if(maxon_slave_ptr->getName()=="Maxonhip42") {
                if(maxon_slave_ptr->lastPdoStateChangeSuccessful() &&
                            maxon_slave_ptr->getReading().getDriveState() == maxon::DriveState::OperationEnabled) {
                            
                        maxon::Command command;
                        command.setModeOfOperation(maxon::ModeOfOperationEnum::CyclicSynchronousTorqueMode);
                        auto reading = maxon_slave_ptr->getReading();
                        command.setTargetTorque(act_torques_leg[1]);

                        maxon_slave_ptr->stageCommand(command);
                        act_states[1] = reading.getActualPosition();
                        act_states[4] = reading.getActualVelocityAveraged();
                
                } else {
                    std::cout << maxon_slave_ptr->getName() << " is not in correct DriveState or the last PDO change was not succesfull" << std::endl;
                }
            } else if(maxon_slave_ptr->getName()=="Maxonknee43") {
                if(maxon_slave_ptr->lastPdoStateChangeSuccessful() &&
                            maxon_slave_ptr->getReading().getDriveState() == maxon::DriveState::OperationEnabled) {
                            
                        maxon::Command command;
                        command.setModeOfOperation(maxon::ModeOfOperationEnum::CyclicSynchronousTorqueMode);
                        auto reading = maxon_slave_ptr->getReading();
                        command.setTargetTorque(act_torques_leg[2]);

                        maxon_slave_ptr->stageCommand(command);
                        act_states[2] = reading.getActualPosition();
                        act_states[5] = reading.getActualVelocityAveraged();

                } else {
                    std::cout << maxon_slave_ptr->getName() << " is not in correct DriveState or the last PDO change was not succesfull" << std::endl;
                }
            } else {
                std::cout << "something went wrong with your slave naming" << std::endl;
            }
        }

        msg1.data = act_states;
        pub.publish(msg1);
        ros::spinOnce();
        loop_rate.sleep();
    }
    shutdown();
}

void Worker::leg_pc_spacehoppermsgs() {


    ros::Subscriber sub = n.subscribe("demanded_actuator_positions", 1000, &Worker::callback_leg_spacehoppermsgs, this);
    ros::Publisher pub = n.advertise<spacehopper_msgs::LegActuatorState>("act_states",1000);
    ros::Rate loop_rate(frequency_hardware_interface);
    ros::spinOnce();

    /*
    ** The communication update loop.
    ** This loop is supposed to be executed at a constant rate.
    ** The EthercatMaster::update function incorporates a mechanism
    ** to create a constant rate.
     */

    while(ros::ok()){
        //create actuator state message
        spacehopper_msgs::LegActuatorState leg_actuator_state;
        leg_actuator_state.leg_index = 1;

        /*
        ** Here are the commands coded which are sent to the actuators
        */
        for(const auto & maxon_slave_ptr:slaves)
        {
            if(maxon_slave_ptr->getName()=="Maxonhip1"){
            if (maxon_slave_ptr->lastPdoStateChangeSuccessful() &&
                    maxon_slave_ptr->getReading().getDriveState() == maxon::DriveState::OperationEnabled)
                {
                    //setup command
                    maxon::Command command;
                    command.setModeOfOperation(maxon::ModeOfOperationEnum::CyclicSynchronousPositionMode);
                    command.setTargetPosition(demanded_actuator_state_leg.actuator1.position);

                    //get actuator states
                    auto reading = maxon_slave_ptr->getReading();
                    
                    //send command to motor
                    maxon_slave_ptr->stageCommand(command);
                    
                    //save actuator states
                    spacehopper_msgs::ActuatorState actuator11;
                    
                    actuator11.header.stamp = ros::Time::now();

                    actuator11.actuator_index = 1;

                    actuator11.position = reading.getActualPosition();
                    actuator11.velocity = reading.getActualVelocity();
                    actuator11.velocity_averaged = reading.getActualVelocityAveraged();
                    
                    actuator11.actual_current = reading.getActualCurrent();
                    actuator11.actual_torque = reading.getActualTorque();

                    leg_actuator_state.actuator1 = actuator11;
                }
            }

            else if(maxon_slave_ptr->getName()=="Maxonhip2"){
            if (maxon_slave_ptr->lastPdoStateChangeSuccessful() &&
                    maxon_slave_ptr->getReading().getDriveState() == maxon::DriveState::OperationEnabled)
                {
                    //setup command
                    maxon::Command command;
                    command.setModeOfOperation(maxon::ModeOfOperationEnum::CyclicSynchronousPositionMode);
                    command.setTargetPosition(demanded_actuator_state_leg.actuator2.position);
                    
                    //get actuator states
                    auto reading = maxon_slave_ptr->getReading();
                    
                    //send command to motor
                    maxon_slave_ptr->stageCommand(command);

                    //save actuator states
                    spacehopper_msgs::ActuatorState actuator12;

                    actuator12.header.stamp = ros::Time::now();

                    actuator12.actuator_index = 2;
                    
                    actuator12.position = reading.getActualPosition();
                    actuator12.velocity = reading.getActualVelocity();
                    actuator12.velocity_averaged = reading.getActualVelocityAveraged();
                    
                    actuator12.actual_current = reading.getActualCurrent();
                    actuator12.actual_torque = reading.getActualTorque();

                    leg_actuator_state.actuator2 = actuator12;
                }
            }

            else if(maxon_slave_ptr->getName()=="Maxonknee"){ 
            if (maxon_slave_ptr->lastPdoStateChangeSuccessful() &&
                    maxon_slave_ptr->getReading().getDriveState() == maxon::DriveState::OperationEnabled)
                {
                    //setup command
                    maxon::Command command;
                    command.setModeOfOperation(maxon::ModeOfOperationEnum::CyclicSynchronousPositionMode);
                    command.setTargetPosition(demanded_actuator_state_leg.actuator3.position);

                    //get actuator states
                    auto reading = maxon_slave_ptr->getReading();
                    
                    //send command to motor
                    maxon_slave_ptr->stageCommand(command);
                    
                    //save actuator states
                    spacehopper_msgs::ActuatorState actuator13;

                    actuator13.header.stamp = ros::Time::now();
                    
                    actuator13.actuator_index = 3;

                    actuator13.position = reading.getActualPosition();
                    actuator13.velocity = reading.getActualVelocityAveraged();
                    actuator13.velocity_averaged = reading.getActualVelocityAveraged();
                    
                    actuator13.actual_current = reading.getActualCurrent();
                    actuator13.actual_torque = reading.getActualTorque();

                    leg_actuator_state.actuator3 = actuator13;
                }
            }

            else
            {
                MELO_WARN_STREAM("Maxon '" << maxon_slave_ptr->getName()
                                                                    << "': " << maxon_slave_ptr->getReading().getDriveState());
            }

        }
        pub.publish(leg_actuator_state);
        ros::spinOnce();
        loop_rate.sleep();
    }
    shutdown();
}

void Worker::leg_pc_stdmsgs(){
    
    ros::Subscriber sub = n.subscribe("demanded_act_pos", 1, &Worker::callback_pc_stdmsgs, this);
    ros::Publisher pub = n.advertise<std_msgs::Float32MultiArray>("act_states", 1);
    ros::Rate loop_rate(frequency_hardware_interface);

    //mainloop
    while(ros::ok()){
        //create actuator state message
        std_msgs::Float32MultiArray msg1;
        std::vector<float> act_states(18);

        for(const auto & maxon_slave_ptr:slaves) 
        {                 
                if(maxon_slave_ptr->getName()=="Maxonhip41"){
                    if (maxon_slave_ptr->lastPdoStateChangeSuccessful() &&
                            maxon_slave_ptr->getReading().getDriveState() == maxon::DriveState::OperationEnabled)
                    {
                        //setup command
                        maxon::Command command;
                        command.setModeOfOperation(maxon::ModeOfOperationEnum::CyclicSynchronousPositionMode);
                        command.setTargetPosition(demanded_act_pos[0]);

                        //get actuator states
                        auto reading = maxon_slave_ptr->getReading();
                        
                        //send command to motor
                        maxon_slave_ptr->stageCommand(command);
                        act_states[0] = reading.getActualPosition();
                        act_states[3] = reading.getActualVelocityAveraged();
                    }
                }

                else if(maxon_slave_ptr->getName()=="Maxonhip42"){
                    if (maxon_slave_ptr->lastPdoStateChangeSuccessful() &&
                            maxon_slave_ptr->getReading().getDriveState() == maxon::DriveState::OperationEnabled)
                        {
                            //setup command
                            maxon::Command command;
                            command.setModeOfOperation(maxon::ModeOfOperationEnum::CyclicSynchronousPositionMode);
                            command.setTargetPosition(demanded_act_pos[1]);

                            //get actuator states
                            auto reading = maxon_slave_ptr->getReading();
                            
                            //send command to motor
                            maxon_slave_ptr->stageCommand(command);
                            act_states[1] = reading.getActualPosition();
                            act_states[4] = reading.getActualVelocityAveraged();
                        }
                }

                else if(maxon_slave_ptr->getName()=="Maxonknee43"){
                    if (maxon_slave_ptr->lastPdoStateChangeSuccessful() &&
                            maxon_slave_ptr->getReading().getDriveState() == maxon::DriveState::OperationEnabled)
                        {
                            //setup command
                            maxon::Command command;
                            command.setModeOfOperation(maxon::ModeOfOperationEnum::CyclicSynchronousPositionMode);
                            command.setTargetPosition(demanded_act_pos[2]);

                            //get actuator states
                            auto reading = maxon_slave_ptr->getReading();
                            
                            //send command to motor
                            maxon_slave_ptr->stageCommand(command);
                            act_states[2] = reading.getActualPosition();
                            act_states[5] = reading.getActualVelocityAveraged();
                        }
                }
                
                else
                {
                    MELO_WARN_STREAM("Maxon '" << maxon_slave_ptr->getName()
                                                                        << "': " << maxon_slave_ptr->getReading().getDriveState());
                }
        }
       
        msg1.data = act_states;
        pub.publish(msg1);
        ros::spinOnce();
        loop_rate.sleep();
    }
    shutdown();
}

void Worker::leg_pc_delay(){
    //counter
    uint i = 0;
    uint step_time = 3200;
    double step_size = 10;
    
    //publishers
    ros::Publisher pub_m1_demand = n.advertise<sensor_msgs::JointState>("m1_demand", 10);
    ros::Publisher pub_m1_actual = n.advertise<sensor_msgs::JointState>("m1_actual", 10);
    ros::Publisher pub_m2_demand = n.advertise<sensor_msgs::JointState>("m2_demand", 10);
    ros::Publisher pub_m2_actual = n.advertise<sensor_msgs::JointState>("m2_actual", 10);
    ros::Publisher pub_m3_demand = n.advertise<sensor_msgs::JointState>("m3_demand", 10);
    ros::Publisher pub_m3_actual = n.advertise<sensor_msgs::JointState>("m3_actual", 10);
    
    //rate
    ros::Rate loop_rate(frequency_hardware_interface);

    //mainloop
    while(ros::ok()){
        //create actuator state message
        sensor_msgs::JointState msg_m1_demand;
        sensor_msgs::JointState msg_m1_actual;
        sensor_msgs::JointState msg_m2_demand;
        sensor_msgs::JointState msg_m2_actual;
        sensor_msgs::JointState msg_m3_demand;
        sensor_msgs::JointState msg_m3_actual;

        for(const auto & maxon_slave_ptr:slaves) 
        {                 
                if(maxon_slave_ptr->getName()=="Maxonhip41"){
                    if (maxon_slave_ptr->lastPdoStateChangeSuccessful() &&
                            maxon_slave_ptr->getReading().getDriveState() == maxon::DriveState::OperationEnabled)
                    {
                        double cmd = 0;
                        if (i > step_time) cmd = step_size;
                        
                        //setup command
                        maxon::Command command;
                        command.setModeOfOperation(maxon::ModeOfOperationEnum::CyclicSynchronousPositionMode);
                        command.setTargetPosition(cmd);

                        //get actuator states
                        auto reading = maxon_slave_ptr->getReading();
                        msg_m1_actual.header.stamp = ros::Time::now();
                        
                        //send command to motor
                        maxon_slave_ptr->stageCommand(command);
                        msg_m1_demand.header.stamp = ros::Time::now();

                        msg_m1_actual.position = std::vector<double> {reading.getActualPosition()};
                        msg_m1_demand.position = std::vector<double> {cmd};
                    }
                }

                else if(maxon_slave_ptr->getName()=="Maxonhip42"){
                    if (maxon_slave_ptr->lastPdoStateChangeSuccessful() &&
                            maxon_slave_ptr->getReading().getDriveState() == maxon::DriveState::OperationEnabled)
                        {
                            double cmd = 0;
                            if (i > step_time) cmd = step_size;
                            
                            //setup command
                            maxon::Command command;
                            command.setModeOfOperation(maxon::ModeOfOperationEnum::CyclicSynchronousPositionMode);
                            command.setTargetPosition(cmd);

                            //get actuator states
                            auto reading = maxon_slave_ptr->getReading();
                            msg_m2_actual.header.stamp = ros::Time::now();
                            
                            //send command to motor
                            maxon_slave_ptr->stageCommand(command);
                            msg_m2_demand.header.stamp = ros::Time::now();

                            msg_m2_actual.position = std::vector<double> {reading.getActualPosition()};
                            msg_m2_demand.position = std::vector<double> {cmd};
                        }
                }

                else if(maxon_slave_ptr->getName()=="Maxonknee43"){
                    if (maxon_slave_ptr->lastPdoStateChangeSuccessful() &&
                            maxon_slave_ptr->getReading().getDriveState() == maxon::DriveState::OperationEnabled)
                        {
                            double cmd = 0;
                            if (i > step_time) cmd = step_size;
                            
                            //setup command
                            maxon::Command command;
                            command.setModeOfOperation(maxon::ModeOfOperationEnum::CyclicSynchronousPositionMode);
                            command.setTargetPosition(cmd);

                            //get actuator states
                            auto reading = maxon_slave_ptr->getReading();
                            msg_m3_actual.header.stamp = ros::Time::now();
                            
                            //send command to motor
                            maxon_slave_ptr->stageCommand(command);
                            msg_m3_demand.header.stamp = ros::Time::now();

                            msg_m3_actual.position = std::vector<double> {reading.getActualPosition()};
                            msg_m3_demand.position = std::vector<double> {cmd};
                        }
                }
                
                else
                {
                    MELO_WARN_STREAM("Maxon '" << maxon_slave_ptr->getName()
                                                                        << "': " << maxon_slave_ptr->getReading().getDriveState());
                }
        }
       
        pub_m1_demand.publish(msg_m1_demand);
        pub_m1_actual.publish(msg_m1_actual);
        pub_m2_demand.publish(msg_m2_demand);
        pub_m2_actual.publish(msg_m2_actual);
        pub_m3_demand.publish(msg_m3_demand);
        pub_m3_actual.publish(msg_m3_actual);
        ros::spinOnce();
        loop_rate.sleep();

        i++;
    }
    shutdown();
}