#include <ros/ros.h>
#include "std_msgs/Float32MultiArray.h"
#include <spacehopper_msgs/ActuatorState.h>
#include <spacehopper_msgs/LegActuatorState.h>
#include <spacehopper_msgs/RobotActuatorState.h>
#include <maxon_epos_ethercat_sdk/Maxon.hpp>
#include "hardware_interface/EthercatDeviceConfigurator.hpp"
#include <vector>
#include <string>


class Worker {
    public:
        //constructor
        Worker(std::string commandline_argument);

        //member variables
        std::string mode;
        std::string config_path;
        std::string commandline_arg;
        ros::NodeHandle n;
        float frequency_hardware_interface;
        float time_step_ethercat_communication;
        

        //membervariables for saving callback data
        std::vector<float> act_torques_robot;
        std::vector<float> act_torques_leg;
        std::vector<float> demanded_act_pos;
        spacehopper_msgs::RobotActuatorState demanded_actuator_state_robot;
        spacehopper_msgs::LegActuatorState demanded_actuator_state_leg;

        //parser methods
        void parse_ethercat_setup_file();

        //PDO Communication
        void start_ros_worker();
        void shutdown();
        void setup_slaves();
        void update_master();
        std::unique_ptr<std::thread> ethercatUpdateThread_; 
        std::atomic<bool> abortEthercatUpdate_ = false;
        EthercatDeviceConfigurator::SharedPtr configurator;

        //vector that holds all slave pointers
        std::vector<std::shared_ptr<maxon::Maxon>> slaves;

        //subscriber callbacks
        void callback_robot_stdmsgs(const std_msgs::Float32MultiArray::ConstPtr &msg);
        void callback_robot_spacehoppermsgs(const spacehopper_msgs::RobotActuatorState msg);
        void callback_leg_stdmsgs(const std_msgs::Float32MultiArray::ConstPtr &msg);
        void callback_pc_stdmsgs(const std_msgs::Float32MultiArray::ConstPtr &msg);
        void callback_leg_spacehoppermsgs(const spacehopper_msgs::LegActuatorState msg);

        //worker methods
        void robot_tc_stdmsgs();
        void robot_pc_spacehoppermsgs();
        void robot_pc_stdmsgs();
        void leg_tc_stdmsgs();
        void leg_pc_spacehoppermsgs();
        void leg_pc_stdmsgs();
        void leg_pc_delay();
};